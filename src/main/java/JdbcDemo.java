import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcDemo {

    private static String URL = "jdbc:mysql://localhost:3306/java_jdbc";
    private static String USERNAME = "root";
    private static String PASSWORD = "root";

    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;

    public void createTable() throws SQLException, ClassNotFoundException {
        getConnection();
        String drop = "DROP TABLE IF EXISTS JDBC_TEST;";
        statement.executeUpdate(drop);
        String create = "CREATE TABLE JDBC_TEST (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, fio char (50));";
        statement.executeUpdate(create);
    }

    public void insertIntoTable() throws SQLException, ClassNotFoundException {
        getConnection();
        String query = "INSERT into jdbc_test VALUES (null, 'ZRV'), (null, 'PDO'), (null, 'CAI')";
        statement.executeUpdate(query);
    }

    public void readFromTable() throws SQLException, ClassNotFoundException {
        getConnection();
        String query = "SELECT * FROM jdbc_test";
        resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String fio = resultSet.getString("fio");
            System.out.println(id + "|" + fio);
        }
    }

    public void getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
